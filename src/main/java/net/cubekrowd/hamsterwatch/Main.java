package net.cubekrowd.hamsterwatch;

import java.util.ArrayList;
import java.util.List;

import net.cubekrowd.eventstorageapi.api.EventStorageAPI;
import net.cubekrowd.eventstorageapi.api.event.ESAPIEvent;
import net.cubekrowd.eventstorageapi.common.EventStoragePlugin;
import net.cubekrowd.eventstorageapi.common.command.Messagable;
import net.cubekrowd.eventstorageapi.common.storage.StorageMongoDB;

public class Main {

    public static void main(String[] args) {

        // Init ESAPI
        EventStorageAPI.registerInit(new StorageMongoDB(new EventStoragePlugin() {
            @Override
            public String getVersion() {
                return null;
            }

            @Override
            public void callEvent(ESAPIEvent event, Object... data) {
            }

            @Override
            public List<String> getAuthors() {
                return null;
            }

            @Override
            public List<Messagable> getDebuggers() {
                return new ArrayList<>();
            }

            @Override
            public boolean isDebug() {
                return false;
            }
        }, "127.0.0.1", 27017, "eventstorage", "", ""));
        EventStorageAPI.getStorage().open();

        new WebServer();
    }

}
