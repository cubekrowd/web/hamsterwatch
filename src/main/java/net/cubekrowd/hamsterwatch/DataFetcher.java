package net.cubekrowd.hamsterwatch;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Collectors;

import net.cubekrowd.eventstorageapi.api.EventStorageAPI;

public class DataFetcher {

    private static class DataCacheEntry<V> {
        long time;
        V value;

        public DataCacheEntry(long time, V value) {
            this.time = time;
            this.value = value;
        }
    }

    private static final Map<Integer, DataCacheEntry<Map<String, Integer>>> cacheTNPL = new HashMap<>();
    private static final Map<Integer, DataCacheEntry<Map<String, Integer>>> cacheUNPL = new HashMap<>();

    public static Map<String, Integer> getTotalNetworkPlayerLogins(int year) {
        long now = System.currentTimeMillis();
        if (cacheTNPL.containsKey(year)) {
            DataCacheEntry<Map<String, Integer>> entry = cacheTNPL.get(year);
            if (entry.time > (now - (1000 * 60 * 60))) {
                return entry.value;
            }
        }

        ZoneId zoneUTC = ZoneId.of("UTC");
        var start = ZonedDateTime.of(year, 1, 1, 0, 0, 0, 0, zoneUTC).toInstant();
        var end = ZonedDateTime.of(year + 1, 1, 1, 0, 0, 0, 0, zoneUTC).toInstant();

        var data = new HashMap<String, Integer>();
        {
            // Universal timezone
            DateTimeFormatter yearFormatter = DateTimeFormatter.ofPattern("yyyy").withZone(ZoneId.of("UTC"));
            var stepper = ZonedDateTime.of(year, 1, 1, 0, 0, 0, 0, zoneUTC).toInstant();
            while (Integer.parseInt(yearFormatter.format(stepper)) == year) {
                data.put(stepper.toString().split("T")[0], 0);
                stepper = stepper.plus(1, ChronoUnit.DAYS);
            }
        }

        EventStorageAPI.getStorage().getEntryStream("RedisJoinLeave", "login", start.toEpochMilli(),
        Math.min(end.toEpochMilli(), System.currentTimeMillis())).forEach((e) -> {
            var inst = Instant.ofEpochMilli(e.getTime());
            var date = inst.toString().split("T")[0];
            data.put(date, data.get(date) + 1);
        });

        cacheTNPL.put(year, new DataCacheEntry<>(now, data));
        return data;
    }

    public static Map<String, Integer> getUniqueNetworkPlayerLogins(int year) {
        long now = System.currentTimeMillis();
        if (cacheUNPL.containsKey(year)) {
            DataCacheEntry<Map<String, Integer>> entry = cacheUNPL.get(year);
            if (entry.time > (now - (1000 * 60 * 60))) {
                return entry.value;
            }
        }

        ZoneId zoneUTC = ZoneId.of("UTC");
        var start = ZonedDateTime.of(year, 1, 1, 0, 0, 0, 0, zoneUTC).toInstant();
        var end = ZonedDateTime.of(year + 1, 1, 1, 0, 0, 0, 0, zoneUTC).toInstant();

        var data = new HashMap<String, Set<String>>();
        {
            // Universal timezone
            DateTimeFormatter yearFormatter = DateTimeFormatter.ofPattern("yyyy").withZone(ZoneId.of("UTC"));
            var stepper = ZonedDateTime.of(year, 1, 1, 0, 0, 0, 0, zoneUTC).toInstant();
            while (Integer.parseInt(yearFormatter.format(stepper)) == year) {
                data.put(stepper.toString().split("T")[0], new HashSet<>());
                stepper = stepper.plus(1, ChronoUnit.DAYS);
            }
        }

        EventStorageAPI.getStorage().getEntryStream("RedisJoinLeave", "login", start.toEpochMilli(),
        Math.min(end.toEpochMilli(), System.currentTimeMillis())).forEach((e) -> {
            var inst = Instant.ofEpochMilli(e.getTime());
            var date = inst.toString().split("T")[0];
            data.get(date).add(e.getData().get("u"));
        });

        var result = data.entrySet().stream().collect(Collectors.toMap(me -> me.getKey(), me -> me.getValue().size()));
        cacheUNPL.put(year, new DataCacheEntry<>(now, result));
        return result;
    }
}
