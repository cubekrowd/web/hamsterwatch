package net.cubekrowd.hamsterwatch;

import static spark.Spark.get;
import static spark.Spark.port;
import static spark.Spark.staticFiles;

import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Map.Entry;
import java.util.TreeSet;

import org.json.JSONStringer;

public class WebServer {

    public WebServer() {
        // root is 'src/main/resources', so put files in 'src/main/resources/public'
        staticFiles.location("/public"); // Static files

        port(8080);

        get("/hello", (req, res) -> "Hello World");

        // Universal timezone
        DateTimeFormatter yearFormatter = DateTimeFormatter.ofPattern("yyyy").withZone(ZoneId.of("UTC"));

        get("/api/totalNetworkPlayerLogins", (req, res) -> {
            res.type("application/json");

            var dataNow = DataFetcher.getTotalNetworkPlayerLogins(Integer.parseInt(yearFormatter.format(Instant.now())));
            var dataLast = DataFetcher.getTotalNetworkPlayerLogins(Integer.parseInt(yearFormatter.format(Instant.now())) - 1);

            var stringer = new JSONStringer().object().key("dataset1").array();
            for (String ek : new TreeSet<>(dataNow.keySet())) {
                stringer = stringer.object().key("x").value(ek).key("y").value(dataNow.get(ek)).endObject();
            }
            stringer = stringer.endArray();
            stringer = stringer.key("dataset2").array();
            for (String ek : new TreeSet<>(dataLast.keySet())) {
                stringer = stringer.object().key("x").value(ek).key("y").value(dataLast.get(ek)).endObject();
            }
            return stringer.endArray().endObject().toString();
        });

        get("/api/uniqueNetworkPlayerLogins", (req, res) -> {
            res.type("application/json");

            var dataNow = DataFetcher.getUniqueNetworkPlayerLogins(Integer.parseInt(yearFormatter.format(Instant.now())));
            var dataLast = DataFetcher.getUniqueNetworkPlayerLogins(Integer.parseInt(yearFormatter.format(Instant.now())) - 1);

            var stringer = new JSONStringer().object().key("dataset1").array();
            for (String ek : new TreeSet<>(dataNow.keySet())) {
                stringer = stringer.object().key("x").value(ek).key("y").value(dataNow.get(ek)).endObject();
            }
            stringer = stringer.endArray();
            stringer = stringer.key("dataset2").array();
            for (String ek : new TreeSet<>(dataLast.keySet())) {
                stringer = stringer.object().key("x").value(ek).key("y").value(dataLast.get(ek)).endObject();
            }
            return stringer.endArray().endObject().toString();
        });
    }

}